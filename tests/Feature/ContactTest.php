<?php

namespace Tests\Feature;

use Tests\TestCase;

class ContactTest extends TestCase
{

    public function testNewContact()
    {
        $this->visit('/')
            ->type('John', 'firstName')
            ->type('John', 'lastName')
            ->type('John', 'email')
            ->type('John', 'phoneNumber')
            ->press('Store')
            ->seePageIs('/');
    }

    public function testFetchContacts()
    {
        $this->visit('/fetch-zoho')
            ->seePageIs('/');
    }

    public static function deleteLastRecord($res)
    {
        if (env('APP_ENV') == 'testing') {
            $xml = simplexml_load_string($res->getBody());
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            $idToDelete = $array['result']['recorddetail']['FL']['0'];
            $deleteUrl = 'https://crm.zoho.eu/crm/private/xml/Contacts/deleteRecords?authtoken=' . env('ZOHO_TOKEN') . '&scope=crmapi&id=' . $idToDelete;
            $client = new \GuzzleHttp\Client();
            $client->request('GET', $deleteUrl);
        }
    }
}
