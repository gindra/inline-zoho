# README #
### What is this repository for? ###

* INLINE ZOHO-API TEST

### How do I get set up? ###

* Clone project (git clone git@bitbucket.org:gindra/inline-zoho.git .)
* Fill .env file with your variables (fill ZOHO_TOKEN with zoho token from crm.zoho.com backend)
* Run composer update
* Run php artisan migrate
* Run phpunit for tests


### Who do I talk to? ###

* info@procreo.eu