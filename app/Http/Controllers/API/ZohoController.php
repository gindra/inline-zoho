<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use Tests\Feature\ContactTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreZohoContact;


class ZohoController extends Controller
{
    /**
     * @param StoreZohoContact $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insert(StoreZohoContact $request)
    {
        $xml = $this->generate_xml($request);
        $this->storeData($xml);

        return redirect()->back()->with('status', 'Inserted into ZOHO');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getContacts()
    {
        $url = 'https://crm.zoho.eu/crm/private/json/Contacts/getRecords?authtoken=' . env('ZOHO_TOKEN') . '&scope=crmapi';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $data = json_decode($res->getBody());
        $message = $this->storeContacts($data);

        return redirect()->back()->with('status', $message);
    }

    /**
     * @param $data
     * @return string
     */
    private function storeContacts($data)
    {
        if (!empty($data->response->nodata))
            return 'No Data';
        $results = $data->response->result->Contacts->row;
        if (!empty($results)) {
            $zohoIds = [];
            if (!empty($results->no) && $results->no == 1) {
                $data = $this->formatData($results);
                Contact::updateOrCreate(['zoho_contact_id' => $data['zoho_contact_id']], $data);
                array_push($zohoIds, $data['zoho_contact_id']);
            } else { // If there is more than one record from API
                foreach ($results as $result) {
                    $data = $this->formatData($result);
                    Contact::updateOrCreate(['zoho_contact_id' => $data['zoho_contact_id']], $data);
                    array_push($zohoIds, $data['zoho_contact_id']);
                }
            }
//          Delete contacts which are delete in zohoCRM
            Contact::whereNotIn('zoho_contact_id', $zohoIds)->delete();
        }

        return 'DB Updated';
    }

    /**
     * @param $result
     * @return array
     */
    private function formatData($result)
    {
        $data = [];
        foreach ($result->FL as $row) {
            if ($row->val == 'CONTACTID') {
                $data['zoho_contact_id'] = $row->content;
            } elseif ($row->val == 'First Name') {
                $data['first_name'] = $row->content;
            } elseif ($row->val == 'Last Name') {
                $data['last_name'] = $row->content;
            } elseif ($row->val == 'Email') {
                $data['email'] = $row->content;
            } elseif ($row->val == 'Phone') {
                $data['phone_number'] = $row->content;
            }
        }

        return $data;
    }

    /**
     * @param $request
     * @return string
     */
    private function generate_xml($request)
    {
        $xml = '
            <Contacts>
                <row no="1">
                    <FL val="First Name">' . $request->firstName . '</FL>
                    <FL val="Last Name">' . $request->lastName . '</FL>
                    <FL val="Email">' . $request->email . '</FL>
                    <FL val="Phone">' . $request->phoneNumber . '</FL>
                </row>
            </Contacts>';

        return $xml;
    }

    /**
     * @param $xml
     * @return bool
     */
    private function storeData($xml)
    {
        $url = 'https://crm.zoho.eu/crm/private/xml/Contacts/insertRecords?authtoken=' . env('ZOHO_TOKEN') . '&scope=crmapi&newFormat=1&xmlData=' . urlencode($xml);
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url);
//        Delete testing data
        ContactTest::deleteLastRecord($res);

        return true;
    }

}