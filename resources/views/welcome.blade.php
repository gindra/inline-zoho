<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ZOHO API TEST</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            ZOHO API TEST
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{route('get-zoho')}}" class="btn btn-default">Fetch</a>
                </p>

                @if(session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
            </div>
        </div>
        <form class="form-horizontal" action="{{route('insert-zoho')}}">
            <div class="form-group {{$errors->has('email')?'has-error':''}}">
                <label for="email" class="control-label col-sm-4">Email address:</label>
                <div class="col-sm-8">
                    <input type="email" name="email" class="form-control" id="email">
                    @if($errors->has('email'))
                        <span style="color:#a94442">{{$errors->first('email')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group {{$errors->has('firstName')?'has-error':''}}">
                <label for="first-name" class="control-label col-sm-4">First Name:</label>
                <div class="col-sm-8">
                    <input type="text" name="firstName" class="form-control" id="first-name">
                    @if($errors->has('firstName'))
                        <span style="color:#a94442">{{$errors->first('firstName')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group {{$errors->has('lastName')?'has-error':''}}">
                <label for="last-name" class="control-label col-sm-4">Last name:</label>
                <div class="col-sm-8">
                    <input type="text" name="lastName" class="form-control" id="last-name">
                    @if($errors->has('lastName'))
                        <span style="color:#a94442">{{$errors->first('lastName')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group {{$errors->has('phoneNumber')?'has-error':''}}">
                <label for="phone-number" class="control-label col-sm-4">Phone number:</label>
                <div class="col-sm-8">
                    <input type="text" name="phoneNumber" class="form-control" id="phone-number">
                      @if($errors->has('phoneNumber'))
                        <span style="color:#a94442">{{$errors->first('phoneNumber')}}</span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Store">
            </div>
        </form>
    </div>
</div>
</body>
</html>
